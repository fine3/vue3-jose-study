export default {
  GC_ACTIONS: [
    'ap.mobilewallet.cashier.main.v2',
    'ap.mobilewallet.cashier.pay.v2',
    'ap.mobilewallet.cashier.pay.query.v2',
    'alipayplus.mobilewallet.user.login',
    'alipayplus.mobilewallet.user.login.consult.v2', //4
    'alipayplus.mobilewallet.security.otp.send.v2', //5
    'alipayplus.mobilewallet.security.otp.verify.v2', //6
    'ap.mobilewallet.cashier.pay.emailreceipt.v2',
  ],
  GC_PK: '-----BEGIN RSA PRIVATE KEY-----\n' +
    '    MIIEogIBAAKCAQEA1PPGr5siBv83dIdzI8FPJ8mBYI73tG2HgTofJ2MkKUfMbQDq\n' +
    '    IhABn6I6X3+8fjIdBhOWT1BIolWwwMD3DG5TyozKbapjTmKtcpnDZ9bVTP8fOk+7\n' +
    '    5BkkQp7iHjQrWbC1OSXSxXHRmjpv2YtoMdSQwrb/8vAvaAkxbzoK2qmWpYPwRGpR\n' +
    '    czcDSR+Qi5unTrzaxKiiRiHGAvSz8utyYtQgLEIvRfJhnoReC5Gj91wqgFkE86yC\n' +
    '    BV7bPve8eK08ASNGKWt4vpQaIAV2ltoA69mZczb5K0B2UBGDjegZdk7ipi3HGR+3\n' +
    '    clZ6ygt9kvbeAswwaa531qOB6quDlKMrhSJfEwIDAQABAoIBAFJcclTKuC9imA0e\n' +
    '    sf57LmUh/0LcKJuE3Sqy0wfFxV+d4EesWeGQtUALW25CFiHsueL2wmSslVsLr0/t\n' +
    '    kd8h8JBfQY+Gnd/cNF8lbzkDEv4zkd1Ypz/CJY8KqkOqQ7XtrTmMEmfA7KMsxMOr\n' +
    '    skHLh613X6iTEpK3M4JkPXxyqMwcdbvXXYMB5KLOLE3zVUsUdScHKqF1+8agmIea\n' +
    '    tegdjwmLyzMxaZzb6uTA7idAwUx0gQIkw+SxQmmA1q79YTLVS0ZbvLNztBM0iEE3\n' +
    '    6HI4ex5QoqfkxyLVimD0rYpNOYDpthe8N18AYTehcC2zv2WpzWD6+OtxsVpfeBxJ\n' +
    '    t/rIkxECgYEA68xSqVVHJGYsGxcZ6LS/CiDmikcDzIRZXg6UDQ5vcbLaN5E1DKhd\n' +
    '    X+naSVWIIzDSjvyYhWsSCu04E3J81I/rW1McbMCHBDT2lSoZgbNiweXx0Wmp8eWg\n' +
    '    965TZJYAhfzvHBKHesGVSoiMEVgzNanYarrIUgBQd8eLOaAKl+XjU+sCgYEA5zJh\n' +
    '    6sWewKaWGui1dpXvESHXTooQaxImthxiAWyOtJA5KumGbmZ09zyiTpTKUSsgo8/P\n' +
    '    8aPK1pLRuhg3c6vFrV+I6w4TT9dwKtzy+q3tAaPLkvNGUHdcTeAtG5TXuK3btcR3\n' +
    '    02CipPqzlxoSajq93nr+FdrnS/zCPApVIJxC33kCgYAkCXYYqw/UYEkdWsp65iTM\n' +
    '    R3r0ZyYNe6ys002NMUeW+N6g33BcHt8maXcS/h9wQWXstG4a9eIsCVUoo7awRc6S\n' +
    '    hoExnbsEyZNl+KktSV0uhRYQCzYN4ipXsCzOkp5B3AYvJCXQfQVz/qppfDFXYj3R\n' +
    '    EfrLjYC+zaBZjjwQOvIjQQKBgA1bIcD4TaV/eihvzRveqYLp25BT9YLZ95aJqTe/\n' +
    '    17aipkrOVs/F53fo7s9vVQi/XxFXjgG9npgqvybrAzkRcUgEvtolZAQ2w+oV/Ytx\n' +
    '    xMFVfwssNqGrY4+0Jab0QEoQ8SJdCTcS3BJ/0SQng2zTm+hgXpFQphADuAzT4/sQ\n' +
    '    S+SxAoGAVLI3eOjIR+m68UA6ln+Zdo6pYoLu1zrm65pKGSs/+r8AthwxdmxhTizG\n' +
    '    xbawOFjTajk4StmKI9tTITLfJu6lRyqd8r7xGRoIjBLxHt9O4U/C6uhB6wD/roi+\n' +
    '    waewi3u92WeRAtmWHf9eEWAyks7FEPbmY/dGfbcrm2i0cu91FQg=\n' +
    '    -----END RSA PRIVATE KEY-----',
  DEMO_ENCODED: 'eyJhbGciOiJSU0EtT0FFUCIsImVuYyI6IkEyNTZHQ00iLCJraWQiOiJQbFctYkNOWC1JYT' +
    'ZydGRodzR4b3ZQZmRwMllhUFI0UUQ0U3k3TlN4ODhJIn0.XJYolq40hxYsEtnooBfEZRfqd4Q2iib1Qcc9IgoXvtaOvIVgWtUppi0ZKtKO3dVV' +
    'O812B0-tK-_u5CGQCz2y3yutk3uhoHS6WMi3EqT3s8IZJ5iWAYEEM9im51XlVfZQnrSlwtEIpMP7WUqHl79TrnCw8h3ycVwvX-XNUFX12fDOHK' +
    'LcnoHHEmdT5qSM-4LiiZcxfPWWwTOVYTNNqocJ6A5quRD7-px63ZuufMw7ajaSGC2fwx4RDueSPY7_BLidQ4wB-kuXt6F-AF0bd0U09pifd-KD' +
    'EI7qUyqPnEs5rqEyimVw0zIHz1fJ5S98qQlL6eFQipwFyqWGFWPEy5zh7w.xy0ajhrcdY0xCy5I.TTtpapbeV9AbaAIPfCG1WEZ79frzzEaIif' +
    'akDuf0sqZf1xGhmv-VxwPTNSxWI4fR5k9lM41wYzJrBdKlvKiYY56AVxbNYkpF7SnMJDf5LZ9OmHIKc_rqehDP7NmMdmcYp8la6oJ2WJHs77CeU' +
    '1xcFsmQm76dnYzROapLePt7cdYW-fios2SxsCFTYgsg7cB9MifDj9ziJXelkXveHYZCHegSjLLJdiQgMc591mRQf8xtZEHWrqVyy6dITwnn7Vq' +
    'YCcRXkEvbD7mFV360kotvqik8Xj91Qndll3N1OcOI2uZhTnJM_rJ4caUXyMVodPeAEwbFqhmW8oXXmrqqx3AEqrxztK0mNYRf-jN1rB3FE3RNcr' +
    'qzsrpHITV3xrGLa4unUUepj_jvybPxiESbL1aBjrY2iTEZkuW-5vvuhO7wLUblK9yLDapUOgT9WijDjAXlc-4ndCuRZQDEhU3XMBlf0aEYLTOIT9' +
    'DP2JbIBbP4EBRbW51UtLzL9-426gui_WlQc6i6.rbq2g9fuChbqnP8KoYyYSg'
}
