import {createRouter, createWebHashHistory} from 'vue-router'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

const routes = [
  {path: '/:pathMatch(.*)*', redirect: 'SplashPage'},
  {name: 'SplashPage', component: () => import('@/pages/SplashPage')},
  {name: 'HomePage', component: () => import('@/pages/HomePage')},
  {name: 'DecodeDataPage', component: () => import('@/pages/DecodeDataPage')},
  {name: 'CheckPhone', component: () => import('@/pages/CheckPhone')},
  {name: 'SendCodePage', component: () => import('@/pages/SendCodePage')},
  {name: 'EnterCodePage', component: () => import('@/pages/EnterCodePage')},
  {name: 'GCashApiV2', component: () => import('@/pages/GCashApiV2')},
  {name: 'GCashApiV1', component: () => import('@/pages/GCashApiV1')},
]

routes.forEach(route => {
  route.path = route.path || '/' + (route.name || '')
});

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  NProgress.start()
  next()
})

router.afterEach((to, from) => {
  NProgress.done()
})

export default router
