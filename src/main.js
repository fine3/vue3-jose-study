import {createApp} from 'vue'
import App from '@/App'
import router from '@/router'
import mock from '@/mock'

const app = createApp(App)
app.config.globalProperties.$mock = mock
app.use(router)
app.mount('#app')

